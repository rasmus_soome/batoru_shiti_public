import subprocess
import socket
import json
from controller.controller import Controller
import cv2
import sys
import traceback

# Ask the user for Baturo robot's host or IP address
baturoConnect = raw_input("Raspberry Pi's hostname or IP address: ")
keyboard_use = raw_input("Do you want to control the robot with the keyboard of thsi machine?")
connection_established = True
values = {"PLAYER_ID":0, "PLAYER_HEALTH":30}
#tank_img = cv2.imread("tank.png")
health_bgr = [0, 255, 0]

# https://stackoverflow.com/questions/1969240/mapping-a-range-of-values-to-another
def translate(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)

    # Convert the 0-1 range into a value in the right range.
    return int(rightMin + (valueScaled * rightSpan))


def health_bar(input_frame, start_x, start_y, end_x, end_y, thickness=2):
    global values
    global health_bgr

    if values["PLAYER_HEALTH"] < 10:
        health_bgr = [0, 0, 255]
    else:
        health_bgr = [0, 255, 0]

    cv2.rectangle(input_frame, (start_x, start_y), (end_x, end_y), (health_bgr[0], health_bgr[1], health_bgr[2]), thickness)  # Contour
    cv2.rectangle(input_frame, (start_x, start_y), (translate(values["PLAYER_HEALTH"], 0, 30, 400, 600), end_y), (health_bgr[0], health_bgr[1], health_bgr[2]), -thickness)  # Filing

try:
    controller = None
    if keyboard_use.upper() == "Y":
        controller = Controller("KEYBOARD")

    # TODO: Add password protection

    # Connect to the robot's video stream
    try:
        camera = cv2.VideoCapture("http://{}:8090/robot.mjpeg".format(baturoConnect))
        #camera = cv2.VideoCapture(0)

    except Exception as e:
        print("Unable to establish a connection to {}".format(baturoConnect))

    controller_socket = None
    if controller is not None:
        controller_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            controller_socket.connect((socket.gethostbyname(baturoConnect), 8091))
            print("Socket connected to {}".format(baturoConnect))
        except:
            print("Connection refused! Try again!")
            t, v, tb = sys.exc_info()
            raise t, v, tb

    controller_socket.setblocking(0)
    while True:
        ret, frame = camera.read()

        cv2.putText(frame, "x", (320, 240), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

        if connection_established:
            try:
                cv2.putText(frame, "Player " + str(values["PLAYER_ID"]), (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                health_bar(frame, 400, 20, 600, 60, 2)
                if values["PLAYER_HEALTH"] == 0:
                    cv2.putText(frame, "GAME OVER", (140, 260), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2)

            except:
                print("Error loading player ID...")


        # Build a frame and display the video stream
        try:
            cv2.imshow("Baturo Shiti", frame)
        except Exception as e:
            print("Failed to build the video frame! This is most likely a connection issue!")
            break

        if controller_socket is not None:
            reconnect = False
            try:
                controller_socket.send("{\"KEYBOARD\": " + json.dumps(controller.values) + "}")
                recv_data = ""
                try:
                    recv_data = controller_socket.recv(1024)
                except:
                    pass
                #print(recv_data)

                try:
                    recvd_json = json.loads(recv_data)

                    if "OK" in recvd_json:
                        print("Got 'OK' from the server, ready to send another key!")

                    if "RECONNECT" in recvd_json and recvd_json["RECONNECT"] == 1 or reconnect:
                        try:
                            controller_socket.connect((socket.gethostbyname(baturoConnect), 8091))
                            print("Reconnected to keyboard server...")
                            reconnect = False
                        except:
                            print("Could not reconnect to the keyboard server...")
                            reconnect = True

                    if "ID" in recvd_json:
                        values["PLAYER_ID"] = recvd_json["ID"]

                    if "HEALTH" in recvd_json:
                        values["PLAYER_HEALTH"] = recvd_json["HEALTH"]

                except Exception as e:
                    pass

            except Exception as e:
                if "BROKEN PIPE" in str(e).upper():
                    raise e
                print(e)

        # Press 'q' to shut down the video stream
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

except:
    print(traceback.format_exc())
finally:
    print("Shutting down! Goodbye!")
    cv2.destroyAllWindows()
    controller.destroy()
    controller_socket.close()

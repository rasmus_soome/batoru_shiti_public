import json
import numpy as np
import copy
import os

#Written by Rasmus Soome for the basketball robot "DRIFT3R" of team Digi6 for Robotex 2017.
#https://github.com/rsoome/Digi6RX2017/blob/master/settings/SettingsHandler.py
class SettingsHandler:

    def __init__(self, fileLocation, inValues, settingsId="settings"):
        self.settingsId = settingsId
        self.fileLoc = fileLocation
        self.values = {}

        self.keys = []
        self.iterPos = 0

        self.initializeSettings(inValues)


    def readFromFileToDict(self):
        try:
            with open(self.fileLoc, "r") as f:
                read = f.read()
                return json.loads(read)
        except ValueError as e:
            print(e)
            print(str(self.settingsId) + ": Error reading from file. Creating empty settings.")
            return dict()

    def getValue(self, key):
        if str(key).upper() in self.values:
            return self.values[str(key).upper()]
        return None

    def getValues(self, keys):
        returnValues = []
        for key in keys:
            returnValues.append(self.getValue(key))

        return returnValues

    def writeFromDictToFile(self):
        print("Writing " + self.settingsId + " to file.")
        with open(self.fileLoc, "w") as f:
            return f.write(json.dumps(self.values, indent=4, sort_keys=True))

    def addValues(self, inValues):
        settingsAdded = False
        for value in inValues:
            if not str(value).upper() in self.values:
                self.values[str(value).upper()] = 0
                settingsAdded = True

        if settingsAdded:
            print("New values added to " + self.settingsId + ". Default value is 0.")
            print("You might want to consider updating the value before continuing.")

    def initializeSettings(self, inValues):

        try:
            self.values = self.readFromFileToDict()
        except IOError:
            lastSlash = self.fileLoc.rfind("\\")
            sep = "\\"
            if lastSlash == -1:
                sep = "/"
                lastSlash = self.fileLoc.rfind("/")
            if lastSlash != -1 and not os.path.exists(os.getcwd() + sep + self.fileLoc[:lastSlash]):
                os.makedirs(os.getcwd() + sep + self.fileLoc[:lastSlash])
            with open(self.fileLoc, "w"):
                pass
        self.addValues(inValues)
        for key in self.values:
            self.keys.append(key)

    def __setitem__(self, key, item):
        if str(key).upper() in self.values:
            self.values[str(key).upper()] = item

    def __getitem__(self, key):
        if str(key).upper() in self.values:
            return self.values[str(key).upper()]
        return None

    def __iter__(self):
        self.iterPos = 0
        return self

    def __next__(self):
        self.iterPos += 1
        if self.iterPos > len(self.keys):
            raise StopIteration

        return self.keys[self.iterPos - 1]

    def next(self):
        return self.__next__()

    def __contains__(self, item):
        return item in self.values

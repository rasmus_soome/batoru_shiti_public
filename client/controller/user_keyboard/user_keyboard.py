import sys
import socket
import json
from settings.SettingsHandler import SettingsHandler

class UserKeyboard:

    def __init__(self, controller):

        self.running = True

        self.host = ''
        self.port = 8091
        self.maxClients = 1

        self.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("User keyboard socket has been created.")

        try:
            self.serverSocket.bind((self.host, self.port))
            print("Binding successful! Waiting for user to connect.")
        except socket.error:
            print("Binding failed!")
            t, v, tb = sys.exc_info()
            raise t, v, tb

        self.serverSocket.listen(self.maxClients)
        self.connection, self.address = self.serverSocket.accept()
        print("User keyboard connected with " + self.address[0] + ":" + str(self.address[1]))
        self.controller = controller

    def run(self):
        try:
            while self.running:
                msg_client = self.connection.recv(1024)  # Data from client
                try:
                    values = json.loads(msg_client)
                    for value in values:
                        self.controller[value] = values[value]
                        self.connection.send("OK")
                except Exception as e:
                    print(e)

        except Exception as e:
            print(e)
            t, v, tb = sys.exc_info()
            raise t, v, tb

        finally:
            self.destroy()


    def destroy(self):
        self.running = False
        self.serverSocket.close()
        sys.exit()

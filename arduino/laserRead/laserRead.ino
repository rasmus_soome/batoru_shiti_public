#include <Servo.h>

int const NUMBER_OF_ANALOG_SENSORS = 2;

String sensorId[] = {"HITBOX_FRONT", "HITBOX_BACK"}; //Analog0: "HITBOX_FRONT", Analog1: "HITBOX_BACK"
int sensorData[NUMBER_OF_ANALOG_SENSORS];
int hitCntr[NUMBER_OF_ANALOG_SENSORS];
int hit[NUMBER_OF_ANALOG_SENSORS];
int startTime[NUMBER_OF_ANALOG_SENSORS];
int NORMAL[NUMBER_OF_ANALOG_SENSORS];

Servo cannon;
int cannonPosition = 90;
int cannonStep = 10;

int shootStart;
int const MAX_SHOOT_LEN = 5000;
int laserPin = 9;
int cannonPin = 7;

int delta = 15;

String readString = "";

void setup() {
  Serial.begin(115200);
  digitalWrite(laserPin, HIGH);
  pinMode(laserPin, OUTPUT);

  cannon.attach(cannonPin);
  cannon.write(cannonPosition);

  for(int i = 0; i < NUMBER_OF_ANALOG_SENSORS; i++)
  {
    startTime[i] = millis();
  }

  for(int i = 0; i < NUMBER_OF_ANALOG_SENSORS; i++)
  {
    startTime[i] = millis();
  }

  for(int i = 0; i < NUMBER_OF_ANALOG_SENSORS; i++)
  {
    NORMAL[i] = 130;
  }
}

void loop() {

  while (Serial.available()) {
    delay(3);  //delay to allow buffer to fill
    if (Serial.available() >0) {
      char c = Serial.read();  //gets one byte from serial buffer
      readString += c; //makes the string readString
    }
  }

  if (readString.length() >0) {
      readString.toUpperCase();

      String node = getValue(readString, ':', 0);
      double val = getValue(readString, ':', 1).toDouble();

      if(node == "CALIBRATE"){
        calibrate();
      }

      if(node == "FIRE"){
        if(val == 1){
          shootStart = millis();
          digitalWrite(laserPin, LOW);
          delay(1);

        }
        if(val == 0){
          digitalWrite(laserPin, HIGH);
        }
      }

      if(node == "CVERTICAL"){

        moveCannon(val);
      }

      readString = "";
  }

  readAnalogSensors();

  /*if((millis() - startTime)%500 == 0){
    startTime = millis();
    Serial.print("Analog value: ");
    Serial.println(sensorData);
    Serial.print("Normal: ");
    Serial.println(NORMAL);
    delay(1);
  }*/

  if((millis() - shootStart)%MAX_SHOOT_LEN == 0){
    digitalWrite(laserPin, HIGH);
  }

}

void calibrate(){

  for(int j = 0; j < NUMBER_OF_ANALOG_SENSORS; j++){
    int n = 1000; //Number of iterations o read sensor value
    int maxVal = 0; //Maximum value read from sensor

    for(int i = 0; i < n; i++){ //Read sensor value n times, find highest value
      sensorData[j] = analogRead(j); //Get analog value at pin j
      if(sensorData[j] > maxVal){
        maxVal = sensorData[j];
      }
    }
    NORMAL[j] = maxVal + delta; //Set baseline as maxvalue plus previously defined constant.
  }
}

void readAnalogSensors(){

  for(int i = 0; i < NUMBER_OF_ANALOG_SENSORS; i++){

    sensorData[i] = analogRead(i);     // read the input pin

    int lastValue = hit[i]; //Last value for edge detection

    if(sensorData[i] > NORMAL[i] && hitCntr[i] < 101) hitCntr[i]++; //Count until 100 to filter out noise

    if(sensorData[i] < NORMAL[i] && hitCntr[i] > 0) hitCntr[i]--; //Same but in reverse

    if(hitCntr[i] >= 100) hit[i] = 1; //If noise is filtered out, notify that sensor is high

    if(hitCntr[i] <= 0) hit[i] = 0; //Same but in reverse

    if(lastValue == 0 && hit[i] == 1){ //Edge detected, notify master.
      sendMessage(sensorId[i], "1");
    }

    if(lastValue == 1 && hit[i] == 0){ //Same but in reverse
      sendMessage(sensorId[i], "0");
    }
  }

}

void sendMessage(String node, String msg){
  Serial.println("{\"" + node + "\": " + msg + "}");
}

void moveCannon(double spd){

  if(spd > 1){
    spd = 1;
  }

  if(spd < -1){
    spd = -1;
  }

  int newPosition = cannonPosition + (int) spd * cannonStep;
  if(newPosition > 180){
    newPosition = 180;
  }
  if(newPosition < 0){
    newPosition = 0;
  }
  cannonPosition = newPosition;
  cannon.write(newPosition);
}

//Code at https://arduino.stackexchange.com/questions/1013/how-do-i-split-an-incoming-string
String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

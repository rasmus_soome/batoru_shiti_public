#!/usr/bin/python

import socket
import sys
import logging
import numpy as np
from threading import Thread
import json
import time


''' FUNCTIONS '''

running = True

def search_player(client_address):

    logging.info("Searching for {} in database...".format(client_address))

    if not player_db.get(client_address):
        global id_generator
        player_db[client_address] = id_generator
        logging.error("{} not found in database. Adding a new player with ID: {}".format(client_address, id_generator))
        id_generator += 1
    elif player_db.get(client_address):
        logging.info("Found {} with ID: {}".format(client_address, player_db.get(client_address)))

    logging.info("Player database function has finished!")

    return player_db.get(client_address)


def search_health(player_id):

    logging.info("Searching for player {} in HEALTH DATABASE...".format(player_id))

    if not health_db.get(player_id):
        logging.error("Player {} is not in the HEALTH DATABASE! Adding player {} with {} health points!".format(player_id, player_id, max_health))
        health_db[player_id] = 30

    logging.info("Health database function has finished...")

    return health_db.get(player_id)


def decrease_health(data, player_id, connection):
    current_health = health_db.get(player_id)  # Find the player in the health database
    new_health = 0

    print(current_health)

    if data == "HITBOX_FRONT":
        new_health = int(current_health - np.random.normal(5, 1.75))
    elif data == "HITBOX_BACK":
        new_health = int(current_health - np.random.normal(2.5, 1.25))

    if new_health <= 0:
        health_db[player_id] = 0
        print(health_db[player_id])
    else:
        health_db[player_id] = new_health
        print(health_db[player_id])

    send_data("{\"HEALTH\": " + str(health_db[player_id]) + "}", "Player {} health is: {}".format(player_id, health_db[player_id]), connection)


def send_data(data, log_msg, connection):
    connection.send((data + "\n").encode())
    logging.info(log_msg)


def data_receive(id, connection):
    from_server = connection.recv(1024).decode()
    logging.info("Received from ID {}: {}".format(id, from_server))
    return from_server

def data_init(player_id, player_health):
    # Send that ID number to the player.
    while running:
        try:
            send_data("{\"ID_INIT\":  " + str(player_id) + "}",
                      "Informed {} about its identification number.".format(player_id), connection)
            data = data_receive(player_id, connection)
            dataAsJSON = json.loads(data)
            if "ID" in dataAsJSON:
                print("*")
                val = dataAsJSON["ID"]
                print("**")
                if val == player_id:
                    break
        except Exception as e:
            print(e)
            #pass
        time.sleep(1)

    while running:
        try:
            send_data("{\"HEALTH_INIT\":  " + str(player_health) + "}",
                    "Informed {} about health.".format(player_id), connection)
            data = data_receive(player_id, connection)
            dataAsJSON = json.loads(data)
            if "HEALTH" in dataAsJSON:
                val = dataAsJSON["HEALTH"]
                if val == player_health:
                    break
        except Exception as e:
            #print(e)
            pass
        time.sleep(1)



def client_thread(connection, address):

    kill_counter = 0

    # Save client information into a variable
    client_info = address[0] + ":" + str(address[1])

    # Send message to client if connection has been established successfully.
    send_data("Connection established! Welcome to Batoru Shiti!",
              "New thread created and a welcome message sent to player at {}".format(address[0]), connection)

    # Check if player is already in the database. If not, create a new ID.
    player_id = search_player(address[0])

    global running

    player_health = search_health(player_id)

    send_data("Your health is:{}".format(player_health),
            "Informed {} about health.".format(player_id), connection)

    data_init(player_id, player_health)

    # Infinite loop so that the function does not terminate and the thread does not end.
    while running:
        global game_active

        try:
            data = "None"
            try:
                data = data_receive(player_id, connection)
            except socket.error:
                pass

            if len(data) == 0:
                #print("Fixing the endles loop... DIE, LOOP, DIE!")
                break

            if game_active:
                if data == "HITBOX_FRONT" or data == "HITBOX_BACK":
                    print(str(address) + " says: ")
                    print("Hit: " + str(player_id))
                    decrease_health(data, player_id, connection)
                if data == "KILL_SOCKET":
                    send_data("OK", "{} sent an emtpty string. Killing socket...".format(player_id), connection)

                for key in health_db:
                    if health_db[key] == 0:
                        send_data("GAME_OVER:" + str(key), "Game is over!", connection)  # Notify all parties that game is over and who lost (ID)
                        game_active = False  # Figure out how to restart the game

                if not game_active:
                    for key in health_db:
                        health_db[key] = 30
                        data_init(player_id, player_health)
                    game_active = True

        except IOError as e:
            logging.warning("Caught", str(e))
            logging.warning("Closing connection to " + client_info)
            sys.exit()

    logging.info("Closing connection to {}".format(address[0]))


''' MAIN CODE '''

''' DEFAULT VALUES '''
host = ''  # Host uses local private address
port = 8900  # Default port
maxClients = 10
player_db = {}  # Database for player network address and identification code
health_db = {}  # Database for players current health
id_generator = 1  # Default player identification number
max_health = 30
game_active = True


# Setup basic logging system
logging.basicConfig(filename="/var/log/batoru_server.log",
                    level=logging.INFO,
                    format="%(asctime)s %(message)s",
                    datefmt="%m/%d/%Y %I:%M:%S %p |")

# Create a socket (INET, STREAM).
serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
logging.info("Socket has been created!")

# Bind host and port to the newly created socket.
try:
    serverSocket.bind((host, port))
    logging.info("Binding successful!")
except socket.error:
    logging.error("Binding failed!")
    sys.exit()

# Listen to incoming connections. Queue up as many as 5 connections before refusing.
serverSocket.listen(maxClients)

logging.info("Socket is now listening! "
             "IP: 0.0.0.0 "
             "Port: {}".format(port))
serverSocket.setblocking(0)
try:
    # Wait for connections and keep active connections alive.
    while True:
        # Wait to accept a connection - blocking call
        while True:
            try:
                connection, address = serverSocket.accept()
                break
            except Exception as e:
                #print(e)
                pass

        # Display client information
        logging.info("Connected with " + address[0] + ":" + str(address[1]))

        # Start a new thread that takes the 1st argument as a function
        # name to be run, second is the tuple of arguments for the function
        thread = Thread(target=client_thread, args=(connection, address,), name=address[0]+" thread")
        thread.start()
except Exception as e:
    print(e)
finally:
    global running
    print("Closing server.")
    running = False
    # Terminating connection
    serverSocket.close()

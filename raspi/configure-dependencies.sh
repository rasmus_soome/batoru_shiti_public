#!/bin/bash

declare -A fileInputs=(["/etc/lightdm/lightdm.conf"]="xserver-command=/etc/X11/xinit/xserverrc"
                        ["/etc/X11/xinit/xserverrc"]="exec Xvfb :0 -screen 0 1024x768x24"
                        ["/etc/ssh/ssh_config"]="ITS JUST A PRANK BRO")

for file in ${!fileInputs[@]};
do
  searchClause=""
  for word in ${fileInputs[$file]}; do
    searchClause+=$word
    searchClause+=".*"
  done
  #echo $searchClause

  echo "Inserting \"${fileInputs[$file]}\" to $file"

  if !(grep $searchClause $file); then
    echo ${fileInputs[$file]} >> $file
    echo "done"
  fi;
done

declare -A commentOut=(["/etc/X11/xinit/xserverrc"]="exec /usr/bin/X -nolisten tcp \"\$@\""
                        ["/etc/ssh/ssh_config"]="ITS JUST A PRANK BRO")

for file in ${!commentOut[@]};
do

  searchClause=""
  for word in ${commentOut[$file]}; do
    searchClause+=$word
    searchClause+=".*"
  done

  echo "Commenting out \"${commentOut[$file]}\" in $file"

  if grep $searchClause $file; then
    sed -i -- "s|${commentOut[$file]}|\#${commentOut[$file]}|g" $file
  fi;
done

from datetime import datetime

class Timer:

    def __init__(self):
        self.start = None

    def startTimer(self):
        self.start = self.getTimeInMillis()

    def stopTimer(self):
        time = self.getTimePassed()
        self.start = None
        return time

    def getTimePassed(self):
        time = -1
        stop = self.getTimeInMillis()
        if self.start != None:
            time = stop - self.start
        return time

    def getTimeInMillis(self):
        tAsS = str(datetime.now()).split()[1].split(":")[2].split(".")
        milliSeconds = 0
        if len(tAsS) > 1:
            milliSeconds = int(tAsS[1][:3])
        return int(datetime.now().strftime("%s")) * 1000 + milliSeconds

    def reset(self):
        time = self.stopTimer()
        self.startTimer()
        return time

import serial
import time

from timer.Timer import Timer


class Ser:

    def __init__(self, target, baud, timeout, sendfreq):
        self.ser = serial.Serial(port=target, baudrate=baud, timeout=timeout)
        self.sendTimer = Timer()
        self.sendTimer.startTimer()
        self.SENDFREQ = sendfreq
        if not self.ser.isOpen():
            self.ser.open()
            time.sleep(1)

    def __sendBytes(self, cmd):
        cmd += "\n"
        self.ser.write(cmd)

    def readBytes(self):
        if self.ser.in_waiting:
            line = self.ser.readline()
            return line
        return "{\"None\":\"None\"}"

    def closeSerial(self):
        print("Closing serial.")
        if self.ser.isOpen():
            self.ser.close()

    def sendValues(self, values):
        if self.sendingTime():
            for key in values:
                self.__sendBytes(str(key) + ":" + str(values[key]))
                time.sleep(0.05)
            self.sendTimer.reset()
            return True
        return False

    def sendingTime(self):
        if self.sendTimer.getTimePassed() >= 1000/self.SENDFREQ:
            self.sendTimer.reset()
            return True
        return False

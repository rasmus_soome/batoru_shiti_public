from move.move import Move
from controller.controller import Controller
from settings.SettingsHandler import SettingsHandler
from server_comm.socket_Client import SocketClient
from stream.bstream import BStream
from user_comm.UserComm import UserComm
from hw.hw import Hardware
import sys
import traceback
from timer.Timer import Timer
import time
import threading

try:

    overrideTimer = Timer()
    settings = SettingsHandler("settings/main/main.settings", ["CONTROLLER_TYPE"], "mainSettings")
    move = Move()
    hardware = Hardware("ARDUINO")
    print("Hardware connection established.")
    controller = Controller(settings["CONTROLLER_TYPE"])
    print("Controller created.")

    playerStream = BStream()
    playerStream.startServer()
    playerStream.startStream()

    userComm = UserComm()
    if settings["CONTROLLER_TYPE"] == "USER_KEYBOARD":
        userComm.setVirtualKeyboard(controller.controller)

    thread = threading.Thread(target=userComm.run, name="userCommSocketThread")
    thread.start()

    sensorTimers = dict()
    for sensor in hardware.getSensorData():
        sensorTimers[sensor] = Timer()

    serverComm = SocketClient()
    time.sleep(0.5)

    while True:
        if controller["RECONNECT"]:
            print("Reconnecting to server.")
            #serverComm.reconnect()

        if controller["ROT"] != 0:
            move.rotate(controller["ROT"])
        elif controller["speed"] != 0:
            move.move(controller["speed"], controller["direction"])
        elif controller["ROT"] == 0 and controller["SPEED"] == 0:
            move.move(0)
        for key in controller:
            if key in hardware:
                hardware[key] = controller[key]
        hardware.setHardwareValues();

        sensed = hardware.updateSensors()

        if overrideTimer.getTimePassed() == -1:
            for sensor in sensed:
                if sensor != "None":
                    if sensorTimers[sensor].getTimePassed() > 1000 or sensorTimers[sensor].getTimePassed() == -1: #Require 100ms betweew two consecutive hits.
                        if sensed[sensor] == 1: #A rising edge has been detected by the hardware. Notify server.
                            print(sensor + " HIT")
                            sensorTimers[sensor].stopTimer()
                            serverComm.data_send(sensor)

                    if sensed[sensor] == 0: #A falling edge has been detected by the hardware.
                        sensorTimers[sensor].startTimer() #Allow the required time between two conswcutive hits.

            if not serverComm.jsonQ.empty():
                val = serverComm.jsonQ.get()

                if "HEALTH_INIT" in val:
                    print(val)
                    serverComm.data_send("{\"HEALTH\": " + str(val["HEALTH_INIT"]) + "}")
                    val["HEALTH"] = val["HEALTH_INIT"]

                if "ID_INIT" in val:
                    print(val)
                    serverComm.data_send("{\"ID\": " + str(val["ID_INIT"]) + "}")
                    val["ID"] = val["ID_INIT"]

                userComm.setData(val)
                if "HEALTH" in val and val["HEALTH"] <= 0:
                    controller.override()
                    overrideTimer.startTimer()

        if overrideTimer.getTimePassed() <= 2000 and overrideTimer.getTimePassed() > -1:
            controller.controller.updateValues({"ROT" : 1})
        else:
            overrideTimer.stopTimer()
            controller.end_override()

except Exception:
    print("Program exited unexpectedly.")
    print(traceback.format_exc())

finally:
    try:
        playerStream.killStream()
    except Exception as e:
        print(e)
    print("---")

    try:
        controller.destroy()
    except Exception as e:
        print(e)
    print("---")

    try:
        move.stop()
    except Exception as e:
        print(e)
    print("---")

    try:
        settings.writeFromDictToFile()
    except Exception as e:
        print(e)
    print("---")

    try:
        serverComm.destroy()
    except Exception as e:
        print(e)
    print("---")

    try:
        hardware.destroy()
    except Exception as e:
        print(e)
    print("---")

    print("Running threads: " + str(threading.enumerate()))
    print("DONE")
    sys.exit()

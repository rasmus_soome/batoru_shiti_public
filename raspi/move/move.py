import gopigo as go
from SpeedSetter import SpeedSetter
from settings import SettingsHandler

class Move:

    def __init__(self):
        self.moveSettings = SettingsHandler("settings/movement/movement.settings",
                                        ["speed", "leftCoif", "rightCoif"], "movement settings")
        self.speedSetter = SpeedSetter(speed = self.moveSettings["speed"],
                            leftCoif = self.moveSettings["leftCoif"], rightCoif = self.moveSettings["rightCoif"])

    def move(self, speed, direction=0):
        left = 1
        right = 1

        bwd = False

        if speed < 0:
            bwd = True
            speed *= -1

        if direction > 0:
            right -= direction
        if direction < 0:
            left += direction

        self.speedSetter.setSpeed(speed, left, right)

        if bwd:
            go.bwd()
        else:
            go.fwd()

    def rotate(self, direction):
        if direction > 0:
            self.speedSetter.setSpeed(direction, right=1, left=1)
            go.right_rot()
        if direction < 0:
            self.speedSetter.setSpeed(-direction, right=1, left=1)
            go.left_rot()

    def stop(self):
        self.moveSettings.writeFromDictToFile()
        go.stop()

import gopigo as go
import atexit

class SpeedSetter:

	def __init__(self, speed=1, leftCoif=1, rightCoif=1):
		self. leftCoif = leftCoif
		self.rightCoif = rightCoif
		self.speedLimit = speed
		self.setSpeed()
		atexit.register(go.stop)

	def set_speed_as_percentage(self, speed, left, right):
		go.set_right_speed(int(speed * 255 * self.rightCoif*right*self.speedLimit))
		go.set_left_speed(int(speed * 255 * self.leftCoif*left*self.speedLimit))

	def set_speed_direct(self, speed,left,right):
		go.set_right_speed(int(speed * self.rightCoif*left*right*self.speedLimit))
		go.set_left_speed(int(speed * self.leftCoif*right*right*self.speedLimit))


	def setSpeed(self, speed=1,left=1,right=1):
		if speed > 1:
			speed = 1
		self.speed = speed
		self.set_speed_as_percentage(self.speed,left,right)

	def setSpeedDirect(self, speed,left,right):
		self.speed = speed//255
		self.set_speed_direct(speed,left,right)

	def getSpeed(self):
		return self.speed

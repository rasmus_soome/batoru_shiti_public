import socket
import sys
import time
import json
import Queue
from threading import Thread
from settings.SettingsHandler import SettingsHandler

class SocketClient:

    def __init__(self):
        self.running = True
        self.addressSettings = SettingsHandler("settings/server_comm/address.settings",
                                ["SERVER_ADDRESS", "SERVER_PORT"], "server address settings")
        self.jsonQ = Queue.PriorityQueue()

        # Connect to the server
        self.connectServer()
        # Create and start a thread
        thread = Thread(target = self.run, name="SocketThread")
        thread.start()


    def connectServer(self):

        # Create a socket

        try:
            self.game_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            #self.game_socket.setblocking(0)
        except Exception:
            print("{} Failed to create socket!".format(self.current_time()))
            t, v, tb = sys.exc_info()
            self.addressSettings.writeFromDictToFile()
            raise t, v, tb

        print("{} Socket has been created".format(self.current_time()))

        # Try to resolve destination hostname
        try:
            self.server_ip = socket.gethostbyname(self.addressSettings["SERVER_ADDRESS"])
        except Exception:
            print("{} Hostname could not be resolved, exiting...".format(self.current_time()))
            t, v, tb = sys.exc_info()
            self.addressSettings.writeFromDictToFile()
            raise t, v, tb

        print("{} IP address of {} is {}".format(self.current_time(), self.addressSettings["SERVER_ADDRESS"], self.server_ip))

        # Connect to the game server
        try:
            self.game_socket.connect((self.server_ip, self.addressSettings["SERVER_PORT"]))
            print("{} Socket connected to {}".format(self.current_time(), self.server_ip))
        except Exception:
            print("{} Connection refused! Try again!".format(self.current_time()))
            t, v, tb = sys.exc_info()
            self.addressSettings.writeFromDictToFile()
            raise t, v, tb


    def current_time(self):
        local_time = time.strftime('%X')
        return local_time + ":"

    def data_send(self, data):
        print("Sending to server: " + data)
        self.game_socket.send(data)

    def data_receive(self):
        from_server = self.game_socket.recv(1024)
        return from_server

    # Communication with the server. Constantly listen for any incoming data
    # Try to keep the connection alive at any cost
    # Call for functions if required
    def run(self):
        self.game_socket.setblocking(0)
        while self.running:
            try:
                incomingData = self.data_receive()
                if len(incomingData) == 0:
                    self.destroy()

                try:
                    d = json.loads(incomingData)
                    self.jsonQ.put(d)
                    #print(d)
                except:
                    print("{} Received: {}".format(self.current_time(), incomingData))

            except:
                if not self.running:
                    self.destroy()
                    break

    def reconnect(self):
        self.game_socket.close()
        self.game_socket.connect((self.server_ip, self.addressSettings["SERVER_PORT"]))


    def destroy(self):
        try:
            self.data_send("KILL_SOCKET")
        except:
            pass
        self.running = False
        self.addressSettings.writeFromDictToFile()
        self.game_socket.close()
        print("Socket killed.")

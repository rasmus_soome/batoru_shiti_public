import json
from settings.SettingsHandler import SettingsHandler
from arduino import Arduino
from fakeuino import Fakeuino

class Hardware:

    def __init__(self, hardwareControl):

        self.iterPos = 0;

        definedCommands = SettingsHandler("settings/hardware/commands.settings",
                                ["CHORIZONTAL", "CVERTICAL", "FIRE", "CALIBRATE"], "commands hardware")
        definedSensors = SettingsHandler("settings/hardware/sensors.settings",
                                ["HITBOX_FRONT", "HITBOX_BACK"], "sensors")

        definedSensors.writeFromDictToFile()
        definedCommands.writeFromDictToFile()
        self.commands = dict()
        self.keys = []
        self.__sensors = dict()

        if hardwareControl == "ARDUINO":
            self.hardwareController = Arduino()

        if hardwareControl == "FAKEUINO":
            self.hardwareController = Fakeuino()

        for key in definedCommands:
            self.commands[key] = 0
            self.keys.append(key)

        for key in definedSensors:
            self.__sensors[key] = 0

    def setHardwareValues(self):
        self.hardwareController.set(self.commands)

    def updateSensors(self):
        sensorValues = self.hardwareController.getValues()
        returnValues = dict()
        #if "None" not in sensorValues:
        #print sensorValues
        try:
            jsonValues = json.loads(sensorValues)
            for key in jsonValues:
                self.setSensorValue(key, jsonValues[key])
                if "INFO" not in key:
                    returnValues[key] = jsonValues[key]
                else:
                    print(key + ": " + str(jsonValues[key]))
        except ValueError:
            pass
        return returnValues

    def resetSensor(self, sensor):
        while(not self.hardwareController.sendMessage("RESET_" + sensor)):
            pass

    def setSensorValue(self, key, value):
        if str(key) in self.__sensors:
            self.__sensors[key] = value

    def __setitem__(self, key, item):
        if str(key).upper() in self.commands:
            self.commands[str(key).upper()] = item

    def __getitem__(self, key):
        if str(key).upper() in self.__sensors:
            return self.__sensors[str(key).upper()]
        return None

    def __iter__(self):
        self.iterPos = 0
        return self

    def __next__(self):
        self.iterPos += 1
        if self.iterPos > len(self.keys):
            raise StopIteration

        return self.keys[self.iterPos - 1]

    def next(self):
        return self.__next__()

    def __contains__(self, item):
	    #print(item)
	    #print(self.commands)
        #print(item in self.commands)
        return item in self.commands

    def getSensorData(self):
        return self.__sensors

    def destroy(self):
        self.hardwareController.destroy()

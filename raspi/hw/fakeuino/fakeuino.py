from ser.ser import Ser
from settings.SettingsHandler import SettingsHandler
from timer.Timer import Timer
import numpy as np
import json

class Fakeuino:

    def __init__(self):
        self.hitTimer = Timer()
        self.hitBoxes = ["HITBOX_FRONT", "HITBOX_BACK"]
        self.hitDuration = 0
        self.hitNode = -1
        self.beenHit = False

        self.HIT_MIN_DURATION = 200
        self.HIT_MAX_DURATION = 3000

    def set(self, values):
        pass

    def getValues(self):
        if self.hitTimer.getTimePassed() >= self.hitDuration:
            self.hitTimer.stopTimer()
            self.beenHit = False
            self.hitDuration = 0
            return json.dumps({self.hitBoxes[self.hitNode]:0})

        if not self.beenHit:
            self.beenHit = self.generateHit()
            if self.beenHit:
                return json.dumps({self.hitBoxes[self.hitNode]:1})

        return "{\"None\":\"None\"}"

    def generateHit(self):
        hit = np.random.choice([False, True], p=[0.99995,0.00005])
        #print(hit)
        if hit:
            self.hitDuration = np.random.randint(self.HIT_MIN_DURATION, self.HIT_MAX_DURATION)
            self.hitTimer.startTimer()
            self.hitNode = np.random.randint(0,len(self.hitBoxes))
            return True
        return False

    def destroy(self):
        pass

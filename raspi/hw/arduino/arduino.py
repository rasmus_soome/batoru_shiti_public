from ser.ser import Ser
from settings.SettingsHandler import SettingsHandler

class Arduino:

    def __init__(self):
        serSettings = SettingsHandler("settings/arduino/serial.settings",
                        ["PORT", "BAUD", "TIMEOUT", "SENDFREQ"], "arduino serial settings")

        serSettings.writeFromDictToFile()

        try:
            self.ser = Ser(serSettings["PORT"], serSettings["BAUD"],
                            serSettings["TIMEOUT"], serSettings["SENDFREQ"])
        except Exception as e:
            print("Arduino serial: " + str(e))

    def set(self, values):
        #print(values)
        self.ser.sendValues(values)

    def sendMessage(self, message):
        self.ser.sendValues({message : 1})

    def getValues(self):
        return self.ser.readBytes()

    def destroy(self):
        self.ser.closeSerial()

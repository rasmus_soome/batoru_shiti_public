import os
import signal
import subprocess
from settings import SettingsHandler

class BStream:
    runningServer = ''
    runningStream = ''


    def __init__(self):
        self.streamSettings = SettingsHandler("settings/streaming/streaming.settings", ["width", "height", "localhost", "port"], "stream settings")

    def startServer(self):
        try:
            self.runningServer = subprocess.Popen("ffserver -hide_banner -loglevel panic -f /etc/ffserver.conf", stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
        except:
            print("Unable to start streaming server...")

    def startStream(self):
        try:
            self.runningStream = subprocess.Popen("ffmpeg -hide_banner -loglevel panic -f v4l2 -s 640x480 -r 20 -i /dev/video0 http://localhost:8090/robot.ffm", stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
        except:
            print("Unable to start streaming...")

    def killStream(self):

        print("Trying to kill streaming...")

        try:
            os.killpg(os.getpgid(self.runningServer.pid), signal.SIGTERM)
            os.killpg(os.getpgid(self.runningStream.pid), signal.SIGTERM)
            print("Video stream has been killed successfully!")
        except:
            print("Failed to kill streaming... this is very bad!")

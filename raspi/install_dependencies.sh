#!/bin/bash

pip install --upgrade pip

pipDependencies=("pynput" "pyserial" "opencv-python" "numpy")

for dependency in ${pipDependencies[@]};
do
  version="$(pip freeze | grep $dependency)"

  echo $version

  if [ "$version" = "" ]; then
    pip install $dependency
    echo "Install done"
  else
    pip install --upgrade $dependency
    echo "Upgrade done"
  fi;

done


apt update && apt upgrade

aptDependencies=("lubuntu-core" "xvfb" "x11vnc")

# Dependencies for streaming
# apt-get install libgstreamer1.0-0 gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-doc gstreamer1.0-tools

for dependency in ${aptDependencies[@]};
do
  exists="$(apt-cache policy $dependency | grep 'Installed:.*(none)')"
  echo $exists

  if [ "$exists" != "" ]; then
    apt install $dependency
  fi;
done

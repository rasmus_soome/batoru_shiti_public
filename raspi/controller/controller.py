from keyboard import Keyboard
from user_keyboard import UserKeyboard
from override import Override
from threading import Thread
import time
import sys

class Controller:

    def __init__(self, type):


        self.iterPos = 0

        self.keys = ["SPEED", "DIRECTION", "FIRE", "CVERTICAL", "CHORIZONTAL", "CALIBRATE", "ROT", "RECONNECT"]
        self.values = dict()

        for key in self.keys:
            self.values[key] = 0

        self.thread = Thread(target = self.start, args=(type, ), name="ControllerThread")
        self.thread.start()
        print("Controller created.")

    def start(self, type):
        type = type.upper()
        if type == "KEYBOARD":
            self.controller = Keyboard(self)
        elif type == "USER_KEYBOARD":
            self.controller = UserKeyboard(self)
        else:
            raise Exception("No controller named " + type + " defined.")
        self.old_controller = self.controller
        self.controller.run()

    def override(self):
        self.old_controller = self.controller
        self.controller = Override(self)

    def end_override(self):
        self.controller = self.old_controller

    def __setitem__(self, key, item):
        if str(key).upper() in self.values:
            self.values[str(key).upper()] = item

    def __getitem__(self, key):
	#print(self.values)
        if str(key).upper() in self.values:
            return self.values[str(key).upper()]
        return None

    def __iter__(self):
        self.iterPos = 0
        return self

    def __next__(self):
        self.iterPos += 1
        if self.iterPos > len(self.keys):
            raise StopIteration
        return self.keys[self.iterPos - 1]

    def next(self):
        return self.__next__()

    def __contains__(self, item):
        return item in self.values

    def destroy(self):
        self.controller.running = False
        print("CONTROLLER CLOSED")

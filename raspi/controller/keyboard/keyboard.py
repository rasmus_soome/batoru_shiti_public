from pynput import keyboard
import sys
from settings.SettingsHandler import SettingsHandler

class Keyboard:

    def __init__(self, controller):

        self.running = True

        definedKeys = [keyboard.Key.up, keyboard.Key.down, keyboard.Key.left,
                        keyboard.Key.right, keyboard.Key.ctrl, keyboard.Key.space,
                        keyboard.KeyCode.from_char('1'), keyboard.KeyCode.from_char('2'),
                        keyboard.KeyCode.from_char('3'), keyboard.KeyCode.from_char('4'),
                        keyboard.KeyCode.from_char('w'), keyboard.KeyCode.from_char('a'),
                        keyboard.KeyCode.from_char('s'), keyboard.KeyCode.from_char('d'),
                        keyboard.KeyCode.from_char('c')]

        self.definedKeyMapping = SettingsHandler("settings/keyboard/keymap.settings", definedKeys, "keymap settings")
        self.definedOnPress = SettingsHandler("settings/keyboard/onpress.settings", definedKeys, "onpress settings")
        self.definedOnRelease = SettingsHandler("settings/keyboard/onrelease.settings", definedKeys, "onrelease settings")


        self.controller = controller

    def run(self):
        try:
            # Collect events until released
            with keyboard.Listener(
                    on_press=lambda x: self.on_press(x),
                    on_release=lambda x: self.on_release(x)) as self.listener:
                self.listener.join()

        except Exception as e:
            t, v, tb = sys.exc_info()
            raise t, v, tb

        finally:
            self.destroy()

    def on_press(self, key):
        keyMapped = self.definedKeyMapping[key]
        valueMapped = self.definedOnPress[key]
        if keyMapped is not None and valueMapped is not None:
            self.controller[keyMapped] = valueMapped
        else:
            print("Key " + str(key) + " is not mapped.")

        #print(self.controller.values)


    def on_release(self, key):
        keyMapped = self.definedKeyMapping[key]
        valueMapped = self.definedOnRelease[key]
        if keyMapped is not None and valueMapped is not None:
            self.controller[keyMapped] = valueMapped
	return self.running

    def destroy(self):
        self.definedKeyMapping.writeFromDictToFile()
        self.definedOnPress.writeFromDictToFile()
        self.definedOnRelease.writeFromDictToFile()
        self.running = False
        self.listener.stop()

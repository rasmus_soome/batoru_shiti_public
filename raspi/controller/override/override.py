import sys
import socket
import json
from settings.SettingsHandler import SettingsHandler

class Override:

    def __init__(self, controller):
        self.controller = controller

    def run(self):
        pass

    def updateValues(self, values):
        for value in values:
            self.controller[value] = values[value]


    def destroy(self):
        pass

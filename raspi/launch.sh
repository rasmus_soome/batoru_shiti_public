#!/bin/bash

#                     brightness (int)    : min=0 max=255 step=1 default=0 value=0 flags=slider
#                       contrast (int)    : min=0 max=255 step=1 default=32 value=32 flags=slider
#                     saturation (int)    : min=0 max=255 step=1 default=64 value=64 flags=slider
#                            hue (int)    : min=-90 max=90 step=1 default=0 value=0 flags=slider
#        white_balance_automatic (bool)   : default=1 value=1
#                       exposure (int)    : min=0 max=255 step=1 default=120 value=120 flags=inactive, volatile
#                 gain_automatic (bool)   : default=1 value=1 flags=update
#                           gain (int)    : min=0 max=63 step=1 default=20 value=20 flags=inactive, volatile
#                horizontal_flip (bool)   : default=0 value=0
#                  vertical_flip (bool)   : default=0 value=0
#           power_line_frequency (menu)   : min=0 max=1 default=0 value=0
#                      sharpness (int)    : min=0 max=63 step=1 default=0 value=0 flags=slider
#
# Written by Rasmus Soome for the basketball robot "DR1FT3R" of team Digi6 for Robotex 2017 basketball competition.
# Edited for the ROBOTICS (LOTI.05.010) course lab 6 by Rasmus Soome.

WB_DEFAULT=1
EXP_DEFAULT=1
FOCUS_DEFAULT=1
FOCUS_CONT_DEFAULT=0
BRIGHTNESS_DEFAULT=128
CONTRAST_DEFAULT=128
SATURATION_DEFAULT=128
SHARPNESS_DEFAULT=128

WB=0
EXP=0
FOCUS=0
FOCUS_CONT=0
BRIGHTNESS=128
CONTRAST=128
SATURATION=128
SHARPNESS=128

echo "Setting up camera."
sudo v4l2-ctl -c white_balance_temperature_auto=$WB
sudo v4l2-ctl -c exposure_auto_priority=$EXP
sudo v4l2-ctl -c focus_auto=$FOCUS
sudo v4l2-ctl -c brightness=$BRIGHTNESS
sudo v4l2-ctl -c contrast=$CONTRAST
sudo v4l2-ctl -c saturation=$SATURATION
sudo v4l2-ctl -c sharpness=$SHARPNESS

echo "Starting program"
sudo python ./${@:1}

echo "Restoring camera settings"
sudo v4l2-ctl -c white_balance_temperature_auto=$WB_DEFAULT
sudo v4l2-ctl -c exposure_auto_priority=$EXP_DEFAULT
sudo v4l2-ctl -c focus_auto=$FOCUS_DEFAULT
sudo v4l2-ctl -c brightness=$BRIGHTNESS_DEFAULT
sudo v4l2-ctl -c contrast=$CONTRAST_DEFAULT
sudo v4l2-ctl -c saturation=$SATURATION_DEFAULT
sudo v4l2-ctl -c sharpness=$SHARPNESS_DEFAULT

echo "Done"

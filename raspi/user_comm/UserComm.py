import sys
import socket
import json

class UserComm:

    def __init__(self, virtualKeyboard = None):

        self.virtualKeyboard = virtualKeyboard

        self.data = {"HEALTH" : 30, "ID" : -1}

        self.running = True

        self.host = ''
        self.port = 8091
        self.maxClients = 1

        self.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("User keyboard socket has been created.")

        try:
            self.serverSocket.bind((self.host, self.port))
            print("Binding successful! Waiting for user to connect.")
        except socket.error:
            print("Binding failed!")
            t, v, tb = sys.exc_info()
            raise t, v, tb

        self.serverSocket.listen(self.maxClients)
        while True:
            try:
                self.connection, self.address = self.serverSocket.accept()
                break
            except:
                if not self.running:
                    self.destroy()
                    break

        print("User keyboard connected with " + self.address[0] + ":" + str(self.address[1]))

    # Added by Tambet
    def data_send(self, data):
        self.serverSocket.send(data)


    def run(self):
        try:
            self.serverSocket.setblocking(0)
            while self.running:

                self.connection.send(json.dumps(self.data))

                try:
                    msg_client = self.connection.recv(1024)  # Data from client
                    try:

                        data = json.loads(msg_client)

                        if "KEYBOARD" in data and self.virtualKeyboard is not None:
                            try:
                                values = data["KEYBOARD"]
                                if self.virtualKeyboard is not None:
                                    self.virtualKeyboard.updateValues(values)
                            except Exception as e:
                                print(e)
                        if "RECONNECT" in data and data["RECONNECT"] == 1:
                            self.connection, self.address = self.serverSocket.accept()


                    except Exception as e:
                        self.connection.send("{\"RECONNECT\" : 1}")
                        self.connection, self.address = self.serverSocket.accept()
                except socket.error:
                    pass

        except Exception as e:
            print(e)
            t, v, tb = sys.exc_info()
            raise t, v, tb

        finally:
            self.destroy()

    def setVirtualKeyboard(self, vkb):
        self.virtualKeyboard = vkb

    def setData(self, data):
        for key in data:
            if str(key) in self.data:
                self.data[str(key)] = data[key]


    def destroy(self):
        print("Destroying user keyboard.")
        self.running = False
        self.serverSocket.close()
        sys.exit()

![Poster](images/poster/poster.png)
# The Tank Game (aka Batoru Shitī)

It's as simple as it gets...

Two tanks (each with 30 lives/health points) enter the battlefield with a mission to blow up the opponent. Each tank has a cannon (laser) to aim and hit enemy's hitboxes until he or she is out of lives.

Tanks will have two sensors: front and back. It's a well-known fact that tanks have a stronger armor in front. Therefore the front sensor has a maximum damage value of 5 and the sensor at the back has a maximum damage value of 10. It should make the game more fun and challenging. Damage values are not static. Instead, they are generated per hit basis. For example, the sensor detects a hit and generates a random value between 2 - 5 and deducts that value from the overall HP variable.

The round will continue until one of the two tanks has run out of lives/health points. Once the tank has run out lives, it stops, does a 720-degree turn to signal defeat and disables itself.

## Phase 1 - Proof of Concept ##
Phase 1 is the assemble, test, re-arrange, make-it-work phase. It should serve as a proof of concept to get all the main ingredients up and running, resolve possible problems and find alternative solutions. If all the base functionality has been implemented, we will move to the second phase and start adding depth to each module.

### Hitbox (light sensor) ###
Hitbox consists of a light sensor (connected to an Arduino) inside of a ping-pong ball. The ball will act as a target and (hopefully) create enough light diffusion that can be picked up by the light sensor. As we are using a red laser, we will wrap the ball inside a red plastic film (filter). If possible, we will try to find and utilize red ping-pong balls. Using a ping-pong ball creates a more prominent target for players to aim at. If there's a positive hit, the Arduino will relay that information to the Raspberry Pi.

### Cannon (laser) ###
Cannon consists of a red laser (650nm 5mW 5V), and its on-off stage is controlled with an NPN transistor. The cannon is connected to a servo able to move the cannon up and down. At this phase, the cannon is attached to the body of the robot. A rotating turret is a possible extra feature that will be developed if there's enough time.

### Control unit (keyboard) ###
The tank is controlled with a wireless keyboard. As the project progresses, there might be slight changes in the overall layout, but from the functionality point of view, all the basics have been laid down.

* UP ARROW - accelerate / forward (gear up)
* DOWN ARROW - recede / reverse (gear down)
* LEFT ARROW - turn left
* RIGHT ARROW - turn right
* CTRL - fire cannon
* SPACE - instant stop
* Z, X, C, V - activate any of the four available power-ups

### Power-ups (image processing) ###
Colored balls will be randomly placed on the battlefield, each of them represents a different power-up that could be picked up and activated. To collect, the player has to drive near to the ball: both image processing and a distance sensor(s) will be used to measure distance and signal a successful pick-up. Once the power-up has been collected, the server is notified, and it will no longer be available for the other player.

At this stage, we hope to implement image processing that can recognize different colors and measure distance. Actual power-up functionality will be added in phase two.

### Server (communication, score keeping) ###
The server has three functions:

* keep live game score
* relay hit/damage information between tanks
* manage power-ups (which power-ups have been collected etc.)

### Health status ###
Three red LEDs are attached to a tank. Each LED signifies ten health points and as the game progresses lights are turned off based on current health. It's just a visual indicator for the player to get some real-time feedback.

## Phase 2 - Developing modules ##

### Head-up-display (HUD) ###
Capture the image from the attached on-board web camera, generate a graphical user interface with OpenCV (current health, available power-ups, speed, crosshair for aiming) and live stream that data from Raspberry Pi to user's personal computer. Raspberry Pi's can be programmed to function as access points. Therefore there is no need for an intermediate server to relay the data and each player can connect to his or her tank.

### User interface ###
Create a user interface in which the HUD can be shown and through which the robot can be controlled, eliminating the need for a wireless keyboard.

###  Power-ups ###
Develop and implement power-ups. For example, extra health, autonomous targeting system, freezing the opponent, etc. Exact functionality will be made up as the project progresses because at this stage it's hard to predict which power-ups would a guarantee balanced gameplay.

## Phase 3(if enough time) - Extra stuff for making stuff extra awesome ##

### Rotating turret ###
This would be freakin' AWESOME! Use A and D to rotate left/right, W and S to aim up or down. This rotating system should also support the web camera so that the players could see what they are aiming at. Probably works on servo motors.

### Playstation/Xbox controller###
Well, the title says it all. Instead of keyboard, use a game controller to control the tank.

### AI ###
Create an AI robot to play against in case you can't find a partner to play with.

## Progress ##

### Robot modules diagram ###
![Robot modules diagram](images/diagrams/project_diagram.png)

### Proof of concept ###
Clicking on the image will open a Youtube video.

[![POC video](https://img.youtube.com/vi/1Se1Y91Iafo/0.jpg)](https://www.youtube.com/watch?v=1Se1Y91Iafo&feature=youtu.be&ab_channel=RasmusSoome)

## Licensing ##

This project is licenced under Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) license

![license](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)

